//第一种方法定义sock2网络环境
//必须写在文件的最前面
//如果工程量大的话 就不能保证Winsock2.h是不是在前面,因此还有第二种写法

#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include<WinSock2.h>
#include<windows.h>
#include <stdio.h>
#pragma comment(lib,"ws2_32.lib")



struct  DataPackage
{
	int age;
	char name[32];
};


int main() {
	//版本号
	WORD ver = MAKEWORD(2, 2);
	WSADATA dat;
	WSAStartup(ver, &dat);
	//----------------
	//--用socketAPl建立简易tcp 客户端
	//1.建立一个socket
	SOCKET _sock=	socket(AF_INET,SOCK_STREAM,0);
	if (INVALID_SOCKET == _sock) {
		printf("错误，建立socket失败");

	
	
	}
	else 
	{
		printf("建立socket成功");

	}

	//2.连接服务器 connect
	sockaddr_in _sin =  {};
	_sin.sin_family = AF_INET;
	_sin.sin_port = htons(4567);
	_sin.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");
	;
	int  ret = connect(_sock, (sockaddr*)&_sin, sizeof(sockaddr_in));
	if (SOCKET_ERROR == ret) {
		printf("错误，连接服务器失败");



	}
	else
	{
		printf("连接服务器成功");

	}
	
	while (true)
	{
		//3.输入请求命令
		char cmdBuf[128] = {};
		scanf("%s", cmdBuf);
		//4.处理请求命令
		if (0 == strcmp(cmdBuf, "exit")) {
			printf("收到，exit任务结束");
			break;
		}
		else
		{
			//5：向服务发送请求
			send(_sock, cmdBuf, strlen(cmdBuf) + 1, 0);

		}
	

	//6.接受服务器信息recv
	char recvBuf[128] = {};
	int nlen=recv(_sock, recvBuf, 128,0);
	if (nlen > 0){
		DataPackage* dp =(DataPackage*)recvBuf;
	printf("接收数据:年龄:=%d,姓名:=%s \n", dp->age,dp->name);
}
	}
	//7.关闭套子节closescoket
	closesocket(_sock);

	//清除Windows socket 环境

	WSACleanup();
	printf("已退出");
	
	getchar();
	return 0;
}